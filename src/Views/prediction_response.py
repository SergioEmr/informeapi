import src.common.constants as const


class PredictionResponse(object):
    def __init__(self, data, cost, training, hyperparameters):
        self.data = data
        self.cost = cost
        self.training = training
        self.hyperparameters = hyperparameters

    def prediction_card(self):
        response = {
            "score": self.data[const.SCORE],
            "cost_function": self.hyperparameters[const.COST_FUNCTION],
            "x1": self.data[const.X1],
            "x2": self.data[const.X2],
            "x3": self.data[const.X3],
            "x4": self.data[const.X4],
            "x5": self.data[const.X5],
            "scale": self.data[const.SCALE]
        }
        return response

    def prediction_report(self):
        response = {
            "score": self.data[const.SCORE],
            "cost_function": self.hyperparameters[const.COST_FUNCTION],
            "error": self.cost[const.ERROR],
            "epochs": [self.training[const.COMPLETE][0], self.training[const.REMAINING][0]],
            "average_error": self.cost[const.AVERAGE_ERROR],
            "heatmap": [self.data[const.X1], self.data[const.X2], self.data[const.X3], self.data[const.X4],
                        self.data[const.X5]],
            "scale": self.data[const.SCALE]
        }
        return response
