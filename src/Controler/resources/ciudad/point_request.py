from flask import request
from flask_restful import Resource
from src.Model.ciudad.accidentes_model import Accidentes
from src.Model.ciudad.delitos_model import Delitos
from src.Views.ciudad.ciudad_response import CiudadResponse


class RequestPoint(Resource):
    def post(self, tipo, layer, inside, element):
        data = request.get_json()
        if tipo == 'accidente':
            return Accidentes.register_delito(data), 202
        elif tipo == 'delito':
            return Delitos.register_delito(data), 202
        else:
            return {"msg": "Error en el endpoint"}, 200

    def get(self, tipo, layer, inside, element):
        if tipo == 'accidente':
            return CiudadResponse(Accidentes.all(layer, inside, element.replace('_', '/'))).geometry_response_all(), 202

        if tipo == 'delito':
            return CiudadResponse(Delitos.all(layer, inside, element.replace('_', ' '))).geometry_response_all(), 202
