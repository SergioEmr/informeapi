import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class TopologyModel(object):
    def __init__(self, model_name, model_version, topology, hidden_layers, neural_network, layer_neuron, layer_function,
                 _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.topology = topology
        self.hidden_layers = hidden_layers
        self.neural_network = neural_network
        self.layer_neuron = layer_neuron
        self.layer_function = layer_function
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_topology(model, version, topology, hidden_layers, neural_network, layer_neuron, layer_function):
        data = DataBase.find_one(const.COLLECTION_TOPOLOGY, {const.MODEL_NAME: model})
        if data is None:
            TopologyModel(model, version, topology, hidden_layers, neural_network, layer_neuron,
                          layer_function).save_to_mongo()
        else:
            try:
                data[const.MODEL_VERSION].index(str(version))
                print(data[const.MODEL_VERSION])
                return {"msg": "versión ya actualizada"}
            except ValueError:
                TopologyModel(model, version, topology, hidden_layers, neural_network, layer_neuron,
                              layer_function).save_to_mongo()

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_TOPOLOGY, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_TOPOLOGY,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_TOPOLOGY, {})]

    @classmethod
    def one(cls, model, version):
        return DataBase.find_one(const.COLLECTION_TOPOLOGY, {const.MODEL_NAME: model, const.MODEL_VERSION: version})

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "topology": self.topology,
            "hidden_layers": self.hidden_layers,
            "neural_network": self.neural_network,
            "layer_neuron": self.layer_neuron,
            "layer_function": self.layer_function
        }
