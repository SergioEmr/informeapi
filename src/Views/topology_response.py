import src.common.constants as const


class TopologyResponse(object):
    def __init__(self, data):
        self.data = data

    def topology_card(self):
        response = {
            "topology": self.data.topology,
            "hidden_layers": self.data.hidden_layers,
            "neural_network": self.data.neural_network
        }
        return response

    def topology_report(self):
        response = {
            "topology": self.data[const.TOPOLOGY],
            "hidden_layers": self.data[const.HIDDEN_LAYERS],
            "neural_network": self.data[const.NEURAL_NETWORK],
            "layer_neuron": self.data[const.LAYER_NEURON],
            "layer_function": self.data[const.LAYER_FUNCTION],
        }
        return response
