import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class ModelsModel(object):
    def __init__(self, model_name, model_versions, model_version_score, _id=None):
        self.model_name = model_name
        self.model_versions = model_versions
        self.model_version_score = model_version_score
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_model(model_name, model_version):
        model = DataBase.find_one(const.COLLECTION_MODELS, {const.MODEL_NAME: model_name})
        if model is None:
            ModelsModel(model_name, [model_version], [const.PENDIENTE]).save_to_mongo()
            return True
        else:
            return False

    @staticmethod
    def register_version(model_name, model_version):
        model = DataBase.find_one(const.COLLECTION_MODELS, {const.MODEL_NAME: model_name})
        if model is None:
            return {"msg": "Ese modelo no existe"}
        else:
            try:
                model.get(const.MODEL_VERSIONS).index(str(model_version))
            except ValueError:
                model[const.MODEL_VERSIONS].append(str(model_version))
                model[const.MODEL_VERSIONS_SCORE].append(str(const.PENDIENTE))
                ModelsModel(model_name, model[const.MODEL_VERSIONS],
                            model[const.MODEL_VERSIONS_SCORE]).update_to_mongo()

    @staticmethod
    def update_version(model_name, model_version, model_score):
        model = DataBase.find_one(const.COLLECTION_MODELS, {const.MODEL_NAME: model_name})
        if model is None:
            return {"msg": "Ese modelo no existe"}
        else:
            try:
                indice = model[const.MODEL_VERSIONS].index(str(model_version))
                model[const.MODEL_VERSIONS_SCORE][indice] = str(model_score)
                ModelsModel(model_name, model[const.MODEL_VERSIONS],
                            model[const.MODEL_VERSIONS_SCORE]).update_to_mongo()
                return True
            except ValueError:
                return False

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_MODELS, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_MODELS,
                        {const.MODEL_NAME: self.model_name}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_MODELS, {})]

    def json(self):
        return {
            "model_name": self.model_name,
            "model_versions": self.model_versions,
            "model_version_score": self.model_version_score
        }
