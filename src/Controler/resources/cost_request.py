from flask import request
from flask_restful import Resource
from src.Views.cost_response import CostResponse
from src.Model.cost_model import CostModel
from src.Model.training_model import TrainingModel
from src.Controler.transaction.hyperparameters import Hyperparameters
import src.common.constants as const


class RequestCost(Resource):
    def get(self, _request, model, version):
        if _request == "card":
            return CostResponse(CostModel.one(model, version), TrainingModel.one(model, version),
                                Hyperparameters.one(model, version)).cost_card(), 202
        if _request == "report":
            return CostResponse(CostModel.one(model, version), TrainingModel.one(model, version),
                                Hyperparameters.one(model, version)).cost_report(), 202

    def post(self, _request, model, version):
        json = request.get_json()
        cost = json[const.COST]
        average_error = json[const.AVERAGE_ERROR]
        error = json[const.ERROR]
        CostModel.update_cost(model, version, cost, error, average_error)
        return {"msg": "Actualizado correctamente"}
