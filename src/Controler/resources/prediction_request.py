from flask import request
from flask_restful import Resource
from src.Views.prediction_response import PredictionResponse
from src.Model.prediction_model import PredictionModel
from src.Model.cost_model import CostModel
from src.Model.training_model import TrainingModel
from src.Controler.transaction.hyperparameters import Hyperparameters
import src.common.constants as const


class RequestPrediction(Resource):
    def get(self, _request, model, version):
        if _request == "card":
            return PredictionResponse(PredictionModel.one(model, version), CostModel.one(model, version),
                                      TrainingModel.one(model, version),
                                      Hyperparameters.one(model, version)).prediction_card(), 202
        if _request == "report":
            return PredictionResponse(PredictionModel.one(model, version), CostModel.one(model, version),
                                      TrainingModel.one(model, version),
                                      Hyperparameters.one(model, version)).prediction_report(), 202

    def post(self, _request, model, version):
        json = request.get_json()
        x1 = json[const.X1]
        x2 = json[const.X2]
        x3 = json[const.X3]
        x4 = json[const.X4]
        x5 = json[const.X5]
        score = json[const.SCORE]
        iteration = json[const.ITERATION]

        PredictionModel.update_prediction(model, version, score, x1, x2, x3, x4, x5, iteration)
        return {"msg": "Correctamente enviado"}, 202
