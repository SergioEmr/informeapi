import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Ageb(object):
    def __init__(self, cvegeo, colonia, cuadrante, properties, geometry, _id=None):
        self.cvegeo = cvegeo
        self.colonia = colonia
        self.cuadrante = cuadrante
        self.properties = properties
        self.geometry = geometry
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_ageb(datos):
        data = DataBase.find_one(const.COLLECTION_AGEB, {const.CVEGEO: datos[const.CVEGEO]})
        if data is None:
            Ageb(datos[const.CVEGEO], datos[const.COLONIA], datos[const.CUADRANTE], datos[const.PROPERTIES],
                 datos[const.GEOMETRY]).save_to_mongo()
            return {"msg": "Registrado correctamente"}
        else:
            return {"msg": "Model o versión no disponible"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_AGEB, self.json())

    @classmethod
    def all(cls, layer, inside, element):
        if inside == 'all':
            return [cls(**x) for x in DataBase.find(const.COLLECTION_AGEB, {const.ALCALDIA: layer})]
        else:
            if inside == 'cuadrante':
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_AGEB, {const.ALCALDIA: layer, const.CUADRANTE: element})]
            elif inside == 'colonia':
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_AGEB, {const.ALCALDIA: layer, const.COLONIA: element})]
            else:
                return {"msg": "Error en el Endpoint"}

    @classmethod
    def one(cls, cvegeo):
        return DataBase.find_one(const.COLLECTION_AGEB, {const.CVEGEO: cvegeo})

    @classmethod
    def one_bycolonia(cls, colonia):
        return DataBase.find_one(const.COLLECTION_AGEB, {const.COLONIA: colonia})

    @classmethod
    def one_bycuadrante(cls, cuadrante):
        return DataBase.find_one(const.COLLECTION_AGEB, {const.CUADRANTE: cuadrante})

    def json(self):
        return {
            const.CVEGEO: self.cvegeo,
            const.COLONIA: self.colonia,
            const.CUADRANTE: self.cuadrante,
            const.PROPERTIES: self.properties,
            const.GEOMETRY: self.geometry
        }
