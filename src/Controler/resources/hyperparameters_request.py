from flask import request
from flask_restful import Resource
from src.Controler.transaction.hyperparameters import Hyperparameters
import src.common.constants as const


class RequestHyperparameters(Resource):
    def post(self, model, version):
        json = request.get_json()
        print(json)
        topology = json[const.TOPOLOGY]
        activation_function = json[const.ACTIVATION_FUNCTION]
        cost_function = json[const.COST_FUNCTION]
        learning_rate = json[const.LEARNING_RATE]
        epochs = json[const.EPOCHS]
        iteration_interval = json[const.ITERATION_INTERVAL]
        error_interval = json[const.ERROR_INTERVAL]
        neural_network = json[const.NEURAL_NETWORK]
        dataset_features = json[const.DATASET_NAME]
        dataset_train_demo = json[const.DATASET_TRAIN_DEMO]
        dataset_test_demo = json[const.DATASET_TEST_DEMO]
        dataset_length = json[const.DATASET_LENGTH]
        dataset_train = json[const.DATASET_TRAIN]
        dataset_test = json[const.DATASET_TEST]

        return Hyperparameters.register_hyperparameters(model, version, topology, activation_function, cost_function,
                                                        learning_rate, epochs, iteration_interval, error_interval,
                                                        neural_network, dataset_features, dataset_train_demo,
                                                        dataset_test_demo, dataset_length, dataset_train,
                                                        dataset_test), 202
