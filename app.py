import os
from flask import Flask
from flask_cors import CORS
from flask_restful import Api

from src.common.ollyreport_database import DataBase

from src.Controler.resources.cost_request import RequestCost
from src.Controler.resources.topology_request import RequestTopology
from src.Controler.resources.training_request import RequestTraining
from src.Controler.resources.prediction_request import RequestPrediction
from src.Controler.resources.recyclerview_request import RequestRecyclerView
from src.Controler.resources.hyperparameters_request import RequestHyperparameters
from src.Controler.resources.dataset_request import RequestDataSet
from src.Controler.resources.files_request import RequestFiles
from src.Controler.resources.models_request import RequestModel
from src.Controler.resources.login_request import RequestLogin
from src.Controler.resources.cndh_request import RequestCndh
from src.Controler.resources.ciudad.ciudad_request import RequestCiudad
from src.Controler.resources.ciudad.browser.city_request_browser import RequestCity
from src.Controler.resources.ciudad.point_request import RequestPoint

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
api = Api(app)


@app.before_first_request
def init_db():
    DataBase.init_database()


# Cost
api.add_resource(RequestCost, "/cost/<string:_request>/<string:model>/<string:version>")
# Topology
api.add_resource(RequestTopology, "/topology/<string:_request>/<string:model>/<string:version>")
# Training
api.add_resource(RequestTraining, "/training/<string:_request>/<string:model>/<string:version>")
# Prediction
api.add_resource(RequestPrediction, "/prediction/<string:_request>/<string:model>/<string:version>")
# RecyclerView
api.add_resource(RequestRecyclerView, "/recyclerview/<string:model>/<string:version>")
# Hyperparameters
api.add_resource(RequestHyperparameters, "/hyperparameters/<string:model>/<string:version>")
# DataSets
api.add_resource(RequestDataSet, "/dataset/<string:model>")
# Resources
api.add_resource(RequestFiles, "/resources/<string:model>/<string:version>/<int:item>")
# Model
api.add_resource(RequestModel, "/model/<string:name>/<string:version>/<string:score>")
# Login
api.add_resource(RequestLogin, "/acces/<string:action>")
# CNDH
api.add_resource(RequestCndh, "/sedena/request")
# ++++++++++++++++ APP +++++++++++++++++++++++++++++++++++++++
# Polygons
api.add_resource(RequestCiudad,
                 "/ciudad/layer=<int:layer>/geometry=<string:geometry>/inside=<string:inside>/element=<string:element>")
# Points
api.add_resource(RequestPoint,
                 "/ciudad/tipo=<string:tipo>/layer=<string:layer>/inside=<string:inside>/element=<string:element>")
# ++++++++++++++ Desktop +++++++++++++++++++++++++++++++++
# Polygons
api.add_resource(RequestCity, "/geometry/estado=<int:estado>/alcaldia=<string:alcaldia>/layer=<string:layer>")

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
