from flask_restful import Resource
from src.Views.topology_response import TopologyResponse
from src.Model.topology_model import TopologyModel


class RequestTopology(Resource):
    def get(self, _request, model, version):
        if _request == "card":
            return TopologyResponse(TopologyModel.all()[0]).topology_card(), 202
        if _request == "report":
            return TopologyResponse(TopologyModel.one(model, version)).topology_report(), 202
