from flask import request
from flask_restful import Resource
from src.Views.training_response import TrainingResponse
from src.Model.training_model import TrainingModel
import src.common.constants as const


class RequestTraining(Resource):
    def get(self, _request, model, version):
        if _request == "card":
            return TrainingResponse(TrainingModel.one(model, version)).training_report(), 202
        if _request == "report":
            return TrainingResponse(TrainingModel.one(model, version)).training_report(), 202

    def post(self, _request, model, version):
        json = request.get_json()
        complete = json[const.COMPLETE]
        remaining = json[const.REMAINING]
        velocity = json[const.VELOCITY]
        memory = json[const.MEMORY]

        TrainingModel.update_training(model, version, complete, remaining, velocity, memory)
        return {"msg": "Correctamente actualizado"}, 202
