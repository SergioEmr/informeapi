from flask_restful import Resource
from src.Model.ciudad.colonias_model import Colonias
from src.Views.ciudad.ciudad_response import CiudadResponse


class RequestCity(Resource):
    def get(self, estado, alcaldia, layer):
        return CiudadResponse(Colonias.all_browser(estado, alcaldia, layer)).geometry_response_all(), 202
