from flask import request
from flask_restful import Resource
from src.Model.login_model import Login
import src.common.constants as const


class RequestLogin(Resource):
    def post(self, action):
        if action == 'register':
            json = request.get_json()
            username = json[const.USERNAME]
            password = json[const.PASSWORD]
            return Login.user_register(username, password)
        elif action == 'login':
            json = request.get_json()
            username = json[const.USERNAME]
            password = json[const.PASSWORD]
            return Login.user_login(username, password)
        else:
            return {'msg': 'Petición invalida'}
