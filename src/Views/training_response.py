import src.common.constants as const


class TrainingResponse(object):
    def __init__(self, data):
        self.data = data

    def training_card(self):
        response = {
            "complete": self.data[const.COMPLETE][1],
            "remaining": self.data[const.REMAINING][1],
            "epochs": self.data.epochs
        }
        return response

    def training_report(self):
        response = {
            "epoch_completes": self.data[const.COMPLETE],
            "epoch_remaining": self.data[const.REMAINING],
            "epochs": [self.data[const.COMPLETE][1], self.data[const.REMAINING][1]],
            "velocity": self.data[const.VELOCITY],
            "memory": self.data[const.MEMORY]
        }
        return response
