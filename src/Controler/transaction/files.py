import os
import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


def save_files(dir_model, files):
    for i in files:
        filename = i.filename
        destination = '/'.join([dir_model, filename])
        i.save(destination)


class Files(object):
    def __init__(self, model_name, model_version, dir_model, path_file, _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.dir_model = dir_model
        self.path_file = path_file
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_model_version(model_name, model_version, dir_model, path_file):
        name = DataBase.find_one(const.COLLECTION_FILES, {const.MODEL_NAME: model_name})
        if name is None:
            if not os.path.isdir(dir_model):
                os.mkdir(dir_model)
            files = []
            for i in path_file:
                files.append(i.filename)
            Files(model_name, model_version, dir_model, files).save_to_mongo()
            save_files(dir_model, path_file)
        else:
            version = DataBase.find_one(const.COLLECTION_FILES,
                                        {const.MODEL_NAME: name[const.MODEL_NAME], const.MODEL_VERSION: model_version})
            if version is None:
                files = []
                for i in path_file:
                    files.append(i.filename)
                Files(name[const.MODEL_NAME], model_version, name[const.MODEL_VERSION], files).save_to_mongo()
                save_files(dir_model, path_file)
            else:
                return {"msg": "Versión ya registrada"}, 202
        return {"msg": "Modelo actualizado"}, 202

    @staticmethod
    def get_resource_version(model, version, item):
        model_version = DataBase.find_one(const.COLLECTION_FILES,
                                          {const.MODEL_NAME: model, const.MODEL_VERSION: version})
        if model_version is None:
            return {"msg": "Versión o modelo inexistente"}
        dir_model = model_version[const.DIR_MODEL]
        print(model_version[const.PATH_FILE])
        file = '/'.join([dir_model, model_version[const.PATH_FILE][item]])
        return file

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_FILES, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_FILES,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def select_model(cls, model):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_FILES, {const.MODEL_NAME: model})]

    @classmethod
    def select_version(cls, model, version):
        return [cls(**x) for x in
                DataBase.find_one(const.COLLECTION_FILES, {const.MODEL_NAME: model, const.MODEL_VERSION: version})]

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "dir_model": self.dir_model,
            "path_file": self.path_file,
        }
