import src.common.constants as const


class RecyclerResponse(object):
    def __init__(self, cost, topology, training, prediction, files, dataset, hyperparameters):
        self.cost = cost
        self.topology = topology
        self.training = training
        self.prediction = prediction
        self.files = files
        self.dataset = dataset
        self.hyperparameters = hyperparameters

    def recyclerview(self):
        response = {
            "topology": self.topology[const.TOPOLOGY],
            "hidden_layers": self.topology[const.HIDDEN_LAYERS],
            "neural_network": self.topology[const.NEURAL_NETWORK],
            "average_error": self.cost[const.AVERAGE_ERROR],
            "cost_function": self.hyperparameters[const.COST_FUNCTION],
            "data": self.cost[const.COST][1:],
            "complete": self.training[const.COMPLETE][1],
            "remaining": self.training[const.REMAINING][1],
            "epochs": [self.training[const.COMPLETE][1], self.training[const.REMAINING][1]],
            "score": self.prediction[const.SCORE],
            "heatmap": [self.prediction[const.X1], self.prediction[const.X2], self.prediction[const.X3],
                        self.prediction[const.X4], self.prediction[const.X5]],
            "scale": self.prediction[const.SCALE],
            "files": ['http://68.183.175.36/resources/' + '/'.join(
                [self.files[0].model_name, self.files[0].model_version, str(x)]) for x in
                      range(len(self.files[0].path_file))],
            "dataset_name": self.dataset[0].dataset_name,
            "dataset_train_demo": self.dataset[0].dataset_train_demo,
            "dataset_test_demo": self.dataset[0].dataset_test_demo,
            "dataset_length": self.dataset[0].dataset_length,
            "description": self.dataset[0].description
        }
        return response
