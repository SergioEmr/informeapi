import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class CostModel(object):
    def __init__(self, model_name, model_version, cost, error, average_error, _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.cost = cost
        self.error = error
        self.average_error = average_error
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_cost(model_name, model_version):
        data = DataBase.find_one(const.COLLECTION_COST,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            CostModel(model_name, model_version, [0], ["0", "0"], const.PENDIENTE).save_to_mongo()
        else:
            return {"msg": "Model o versión no disponible"}

    @staticmethod
    def update_cost(model_name, model_version, cost, error, average_error):
        data = DataBase.find_one(const.COLLECTION_COST,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            return {"msg": "Modelo o versión no disponible"}
        else:
            data[const.COST].append(cost)
            data[const.ERROR] = error
            data[const.AVERAGE_ERROR] = average_error
            CostModel(data[const.MODEL_NAME], data[const.MODEL_VERSION], data[const.COST], data[const.ERROR],
                      data[const.AVERAGE_ERROR]).update_to_mongo()
            return {"msg": "Modelo correctamente actualizado"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_COST, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_COST,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_COST, {})]

    @classmethod
    def one(cls, model, version):
        return DataBase.find_one(const.COLLECTION_COST, {const.MODEL_NAME: model, const.MODEL_VERSION: version})

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "cost": self.cost,
            "error": self.error,
            "average_error": self.average_error
        }
