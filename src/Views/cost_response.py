import src.common.constants as const


class CostResponse(object):
    def __init__(self, data, training, hyperparameters):
        self.data = data
        self.training = training
        self.hyperparameters = hyperparameters

    def cost_card(self):
        response = {
            "average_error": self.data[const.AVERAGE_ERROR],
            "cost_function": self.hyperparameters[const.COST_FUNCTION],
            "data": self.data[const.COST]
        }
        return response

    def cost_report(self):
        response = {
            "data": self.data[const.COST][1:],
            "cost_function": self.hyperparameters[const.COST_FUNCTION],
            "error": self.data[const.ERROR],
            "epochs": [self.training[const.COMPLETE][0], self.training[const.REMAINING][0]],
            "average_error": self.data[const.AVERAGE_ERROR],
            "learning_rate": self.hyperparameters[const.LEARNING_RATE]
        }
        return response
