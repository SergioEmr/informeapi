import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const
from src.Model.models_model import ModelsModel
from src.Model.topology_model import TopologyModel
from src.Model.prediction_model import PredictionModel
from src.Model.training_model import TrainingModel
from src.Model.cost_model import CostModel


class Hyperparameters(object):
    def __init__(self, model_name, model_version, topology, activation_function, cost_function, learning_rate, epochs,
                 iteration_interval, error_interval, neural_network, dataset_name, dataset_train_demo,
                 dataset_test_demo, dataset_length, dataset_train, dataset_test, _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.topology = topology
        self.activation_function = activation_function
        self.cost_function = cost_function
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.iteration_interval = iteration_interval
        self.error_interval = error_interval
        self.neural_network = neural_network
        self.dataset_name = dataset_name
        self.dataset_train_demo = dataset_train_demo
        self.dataset_test_demo = dataset_test_demo
        self.dataset_length = dataset_length
        self.dataset_train = dataset_train
        self.dataset_test = dataset_test
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_hyperparameters(model_name, model_version, topolgy, activation_function, cost_function, learning_rate,
                                 epochs, iteration_interval, error_interval, neural_network, dataset_features,
                                 dataset_train_demo, dataset_test_demo, dataset_length, dataset_train, dataset_test):
        name = DataBase.find_one(const.COLLECTION_HYPERPARAMETERS, {const.MODEL_NAME: model_name})

        if name is None:
            Hyperparameters(model_name, model_version, topolgy, activation_function, cost_function, learning_rate,
                            epochs, iteration_interval, error_interval, neural_network, dataset_features,
                            dataset_train_demo, dataset_test_demo, dataset_length, dataset_train,
                            dataset_test).save_to_mongo()
            hidden_layers = "Capas ocultas: {}".format(str(len(topolgy[1:-1])))
            resume = [str(topolgy[0]), str(topolgy[1]), str(sum(topolgy[2:-2])), str(topolgy[-2]), str(topolgy[-1])]
            TopologyModel.register_topology(model_name, model_version, resume, hidden_layers, neural_network, topolgy,
                                            activation_function)
            ModelsModel.register_model(model_name, model_version)
            PredictionModel.register_prediction(model_name, model_version)
            TrainingModel.register_training(model_name, model_version)
            CostModel.register_cost(model_name, model_version)
            return {"msg": "Hiperparámetros actualizados"}
        else:
            version = DataBase.find_one(const.COLLECTION_HYPERPARAMETERS,
                                        {const.MODEL_NAME: name[const.MODEL_NAME], const.MODEL_VERSION: model_version})
            if version is None:
                Hyperparameters(model_name, model_version, topolgy, activation_function, cost_function, learning_rate,
                                epochs, iteration_interval, error_interval, neural_network, dataset_features,
                                dataset_train_demo, dataset_test_demo, dataset_length, dataset_train,
                                dataset_test).save_to_mongo()
                hidden_layers = "Capas ocultas: {}".format(str(len(topolgy[1:-1])))
                resume = [str(topolgy[0]), str(topolgy[1]), str(sum(topolgy[2:-2])), str(topolgy[-2]), str(topolgy[-1])]
                TopologyModel.register_topology(model_name, model_version, resume, hidden_layers, neural_network,
                                                topolgy, activation_function)
                ModelsModel.register_version(model_name, model_version)
                PredictionModel.register_prediction(model_name, model_version)
                TrainingModel.register_training(model_name, model_version)
                CostModel.register_cost(model_name, model_version)
                return {"msg": "Hiperparámetros actualizados version"}
            else:
                return {"msg": "Versión ya registrada"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_HYPERPARAMETERS, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_HYPERPARAMETERS,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def select_model(cls, model):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_HYPERPARAMETERS, {const.MODEL_NAME: model})]

    @classmethod
    def select_version(cls, model, version):
        return [cls(**x) for x in
                DataBase.find_one(const.COLLECTION_HYPERPARAMETERS,
                                  {const.MODEL_NAME: model, const.MODEL_VERSION: version})]

    @classmethod
    def one(cls, name, version):
        return DataBase.find_one(const.COLLECTION_HYPERPARAMETERS,
                                 {const.MODEL_NAME: name, const.MODEL_VERSION: version})

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "topology": self.topology,
            "activation_function": self.activation_function,
            "cost_function": self.cost_function,
            "learning_rate": self.learning_rate,
            "epochs": self.epochs,
            "iteration_interval": self.iteration_interval,
            "error_interval": self.error_interval,
            "neural_network": self.neural_network,
            "dataset_features": self.dataset_name,
            "dataset_train_demo": self.dataset_train_demo,
            "dataset_test_demo": self.dataset_test_demo,
            "dataset_length": self.dataset_length,
            "dataset_train": self.dataset_train,
            "dataset_test": self.dataset_test
        }
