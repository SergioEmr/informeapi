from flask_restful import Resource
from src.Views.recycler_response import RecyclerResponse
from src.Model.cost_model import CostModel
from src.Model.training_model import TrainingModel
from src.Model.topology_model import TopologyModel
from src.Model.prediction_model import PredictionModel
from src.Controler.transaction.hyperparameters import Hyperparameters
from src.Controler.transaction.files import Files
from src.Controler.transaction.dataset import Dataset


class RequestRecyclerView(Resource):
    def get(self, model, version):
        return RecyclerResponse(CostModel.one(model, version), TopologyModel.one(model, version),
                                TrainingModel.one(model, version), PredictionModel.one(model, version),
                                Files.select_model(model), Dataset.select_model(model),
                                Hyperparameters.one(model, version)).recyclerview(), 202
