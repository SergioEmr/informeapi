from flask import request
from flask_restful import Resource
from src.Model.ciudad.cuadrantes_model import Cuadrantes
from src.Model.ciudad.colonias_model import Colonias
from src.Model.ciudad.red_model import RedVial
from src.Model.ciudad.ageb_model import Ageb
from src.Views.ciudad.ciudad_response import CiudadResponse


class RequestCiudad(Resource):
    def post(self, layer, geometry, inside, element):
        data = request.get_json()
        if geometry == 'cuadrante':
            return Cuadrantes.register_cuadrante(data), 202
        elif geometry == 'colonia':
            return Colonias.registre_colonia(data), 202
        elif geometry == 'red':
            return RedVial.register_red(data), 202
        elif geometry == 'ageb':
            return Ageb.register_ageb(data), 202

    def get(self, layer, geometry, inside, element):
        if geometry == 'cuadrante':
            if element == 'all':
                return CiudadResponse(Cuadrantes.all(layer)).geometry_response_all(), 202
            else:
                return CiudadResponse(Cuadrantes.one(element)).geometry_response(), 202

        if geometry == 'colonias':
            lista = element.split('-')
            if element == 'all':
                return CiudadResponse(Colonias.all(layer, element)).geometry_response_all(), 202
            elif lista[0] == 'cp':
                return CiudadResponse(Colonias.one_bycp(lista[1])).geometry_response(), 202
            elif lista[0] == 'id':
                return CiudadResponse(Colonias.one_byid(lista[1])).geometry_response(), 202
            else:
                return CiudadResponse(Colonias.one_byname(lista[0])).geometry_response(), 202

        if geometry == 'red':
            if element == 'all':
                return CiudadResponse(RedVial.all(layer, inside, element)).geometry_response_all(), 202
            else:
                return CiudadResponse(RedVial.one(element)).geometry_response(), 202

        if geometry == 'ageb':
            if element == 'all':
                return CiudadResponse(RedVial.all(layer, inside, element)).geometry_response_all(), 202
            else:
                return CiudadResponse(RedVial.one(element)).geometry_response(), 202
