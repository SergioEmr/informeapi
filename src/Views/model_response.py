import src.common.constants as const


class ModelResponse(object):
    def __init__(self, data):
        self.data = data

    def getModels(self):
        response = []
        for document in range(len(self.data)):
            print(self.data[document])
            response.append({
                "model_name": self.data[document].model_name,
                "model_versions": self.data[document].model_versions,
                "model_version_score": self.data[document].model_version_score
            })
        return {const.MODELS: response}
