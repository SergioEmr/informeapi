import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Dataset(object):
    def __init__(self, model_name, dataset_name, dataset_train_demo, dataset_test_demo, dataset_length, description, _id=None):
        self.model_name = model_name
        self.dataset_name = dataset_name
        self.dataset_train_demo = dataset_train_demo
        self.dataset_test_demo = dataset_test_demo
        self.dataset_length = dataset_length
        self.description = description
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_dataset(model_name, dataset_name, dataset_train, dataset_test, dataset_lengh, description):
        name = DataBase.find_one(const.COLLECTION_DATASETS, {const.MODEL_NAME: model_name})
        if name is None:
            Dataset(model_name, dataset_name, dataset_train, dataset_test, dataset_lengh, description).save_to_mongo()
            return {"msg": "Set de datos correctamente registrado"}
        else:
            return {"msg": "Set de datos ya registrado para ese modelo"}
        pass

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_DATASETS, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_DATASETS, {const.MODEL_NAME: self.model_name}, self.json())

    @classmethod
    def select_model(cls, model):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_DATASETS, {const.MODEL_NAME: model})]

    def json(self):
        return {
            "model_name": self.model_name,
            "dataset_name": self.dataset_name,
            "dataset_train_demo": self.dataset_train_demo,
            "dataset_test_demo": self.dataset_test_demo,
            "dataset_length": self.dataset_length,
            "description": self.description
        }
