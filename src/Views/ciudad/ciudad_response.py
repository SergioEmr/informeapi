class CiudadResponse(object):
    def __init__(self, data):
        self.data = data

    def geometry_response_all(self):
        response = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}}, "type": "FeatureCollection",
                    "features": []}
        features = []
        for document in range(len(self.data)):
            features.append({"geometry": self.data[document].geometry, "id": self.data[document].identificador,
                             "properties": self.data[document].properties, "type": "Feature"})
        response["features"] = features
        return response

    def geometry_response(self):
        response = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}}, "type": "FeatureCollection",
                    "features": [{"geometry": self.data["geometry"], "properties": self.data["properties"],
                                  "type": "Feature"}]}
        return response
