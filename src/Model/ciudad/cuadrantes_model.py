import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Cuadrantes(object):
    def __init__(self, alcaldia, nomenclatu, properties, geometry, _id=None):
        self.alcaldia = alcaldia
        self.nomenclatu = nomenclatu
        self.properties = properties
        self.geometry = geometry
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_cuadrante(datos):
        data = DataBase.find_one(const.COLLECTION_CUADRANTES, {const.NOMENCLATU: datos[const.NOMENCLATU]})
        if data is None:
            Cuadrantes(datos[const.ALCALDIA], datos[const.NOMENCLATU], datos[const.PROPERTIES],
                       datos[const.GEOMETRY]).save_to_mongo()
            return {"msg": "Registrado correctamente"}
        else:
            return {"msg": "Model o versión no disponible"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_CUADRANTES, self.json())

    @classmethod
    def all(cls, layer):
        if layer == 'null':
            return [cls(**x) for x in DataBase.find(const.COLLECTION_CUADRANTES, {})]
        else:
            return [cls(**x) for x in DataBase.find(const.COLLECTION_CUADRANTES, {const.ALCALDIA: layer})]

    @classmethod
    def one(cls, nomenclatura):
        return DataBase.find_one(const.COLLECTION_CUADRANTES, {const.NOMENCLATU: nomenclatura})

    def json(self):
        return {
            const.ALCALDIA: self.alcaldia,
            const.NOMENCLATU: self.nomenclatu,
            const.PROPERTIES: self.properties,
            const.GEOMETRY: self.geometry
        }
