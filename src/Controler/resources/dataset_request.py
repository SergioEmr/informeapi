from flask import request
from flask_restful import Resource
from src.Controler.transaction.dataset import Dataset
import src.common.constants as const


class RequestDataSet(Resource):
    def post(self, model):
        json = request.get_json()
        dataset_name = json[const.DATASET_NAME]
        dataset_train_demo = json[const.DATASET_TRAIN_DEMO]
        dataset_test_demo = json[const.DATASET_TEST_DEMO]
        dataset_length = json[const.DATASET_LENGTH]
        description = json[const.DESCRPTION]
        return Dataset.register_dataset(model, dataset_name, dataset_train_demo, dataset_test_demo, dataset_length,
                                        description), 202
