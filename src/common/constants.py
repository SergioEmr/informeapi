# +++++++++++ COST +++++++++++++++#
COLLECTION_COST = "Cost"
COST = "cost"
AVERAGE_ERROR = "average_error"
ERROR = "error"

# ++++++++++ TOPOLOGY +++++++++++#
COLLECTION_TOPOLOGY = "Topology"
HIDDEN_LAYERS = "hidden_layers"
LAYER_NEURON = "layer_neuron"
LAYER_FUNCTION = "layer_function"

# ++++++++++ TRAINING +++++++++++#
COLLECTION_TRAINING = "Training"
COMPLETE = "complete"
REMAINING = "remaining"
VELOCITY = "velocity"
MEMORY = "memory"

# ++++++++++ PREDICTION +++++++++#
COLLECTION_PREDICTION = "Prediction"
SCORE = "score"
X1 = "x1"
X2 = "x2"
X3 = "x3"
X4 = "x4"
X5 = "x5"
ITERATION = "iteration"
SCALE = "scale"

# ++++++++++ HYPERPARAMETERS +++++++#
COLLECTION_HYPERPARAMETERS = "Hyperparameters"
TOPOLOGY = "topology"
ACTIVATION_FUNCTION = "activation_function"
COST_FUNCTION = "cost_function"
LEARNING_RATE = "learning_rate"
EPOCHS = "epochs"
ITERATION_INTERVAL = "iteration_interval"
ERROR_INTERVAL = "error_interval"
NEURAL_NETWORK = "neural_network"
DATASET_TRAIN = "dataset_train"
DATASET_TEST = "dataset_test"

# ++++++++++ FILES +++++++++++++#
COLLECTION_FILES = "Files"
PATH_FILE = "path_file"
DIR_MODEL = "dir_model"
# +++++++++++ DATASETS ++++++++++++#
COLLECTION_DATASETS = "Datasets"
DATASET_NAME = "dataset_name"
DATASET_TRAIN_DEMO = "dataset_train_demo"
DATASET_TEST_DEMO = "dataset_test_demo"
DATASET_LENGTH = "dataset_length"
DESCRPTION = "description"

# +++++++++++ MODELS ++++++++++++++++#
COLLECTION_MODELS = "Models"
MODEL_VERSIONS = "model_versions"
MODEL_VERSIONS_SCORE = "model_version_score"
MODELS = "models"

# ++++++++++ COMMON +++++++++++++#
MODEL_NAME = "model_name"
MODEL_VERSION = "model_version"
PENDIENTE = "pendiente"

# +++++++++++ USERS +++++++++++++#
COLLECTION_USERS = "Users"
USERNAME = "username"
PASSWORD = "password"

# +++++++++++ DATA +++++++++++++++#
COLLECTION_CNDH = "CNDH"
FECHA_1 = "fecha_1"
STATUS = "status"
DENUNCIANTE = "denunciante"
DIR_DENUNCIANTE = "dir_denunciante"
PARENTEZCO = "parentezco"
DIR_AGRAVIADO = "dir_agraviado"
TELEFONO = "telefono"
CARGO_PERSONA = "cargo_persona"
AGRAVIADO = "agraviado"
EDAD = "edad"
SEXO = "sexo"
NACIONALIDAD = "nacionalidad"
AUTORIDADES_RESPONSABLES = "autoridades_responsables"
HORA_HECHOS = "hora_hechos"
FECHA_HECHOS = "fecha_hechos"
ENTIDAD = "entidad"
MUNICIPIO = "municipio"
LUGAR_HECHOS = "lugar_hechos"
NUM_PERSONAS = "num_personas"
SUJETOS_UNIFORMADOS = "sujetos_uniformados"
SUJETOS_ARMAS = "sujetos_armas"
VEHICULO_ACTIVO = "vehiculo_activo"
DESCRIPCION_VEHICULO = "descripcion_vehiculo"
ACTIVOS_DELINCUENCIA = "activos_delincuencia"
PASIVO_DELINCUENCIA = "pasivo_delincuencia"
NARRACION = "narracion"
LAT = "lat"
LNG = "lng"
# ++++++++++ MyCity ++++++++++++++++++++++ #
COLLECTION_CUADRANTES = 'cuadrantes'
COLLECTION_DELITOS = 'delitos'
COLLECTION_ACCIDENTES = 'accidentes'
COLLECTION_ALCALDIA = "alcaldia"
NOMENCLATU = "nomenclatu"
PROPERTIES = "properties"
TYPE = "type"
ALCALDIA = "alcaldia"
ESTADO = "estado"
CVE_ZONA = "cve_zona"
AREA = "area_km2"
NO_REGIONS = "no_regions"
FEATURES = "features"
GEOMETRY = "geometry"
COORDINATES = "coordinates"
CUADRANTE = 'cuadrante'
FOLIO = "folio"
# +++++++++++ COLONIA ++++++++++++++++++++#
COLLECTION_COLONIAS = 'colonias'
NOMBRE = "nombre"
COLONIA = "colonia"
IDENTIFICADOR = "identificador"
CP = "cp"
# +++++++++++ RED VIAL ++++++++++++++++++++#
COLLECTION_RED = "redvial"
CVEVIAL = "cvevial"
# +++++++++++ AGEB +++++++++++++++++++++++#
COLLECTION_AGEB = "ageb"
CVEGEO = "cvegeo"
