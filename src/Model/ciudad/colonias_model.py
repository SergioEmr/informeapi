import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Colonias(object):
    def __init__(self, nombre, estado, alcaldia, identificador, cp, properties, geometry, _id=None):
        self.nombre = nombre
        self.alcaldia = alcaldia
        self.estado = estado
        self.identificador = identificador
        self.cp = cp
        self.properties = properties
        self.geometry = geometry
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def registre_colonia(datos):
        data = DataBase.find_one(const.COLLECTION_COLONIAS, {const.IDENTIFICADOR: datos[const.IDENTIFICADOR]})
        if data is None:
            Colonias(datos[const.NOMBRE], datos[const.ALCALDIA], datos[const.IDENTIFICADOR], datos[const.CP],
                     datos[const.PROPERTIES], datos[const.GEOMETRY]).save_to_mongo()
            return {"msg": "Registrado correctamente"}
        else:
            return {"msg": "Model o versión no disponible"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_COLONIAS, self.json())

    @classmethod
    def all(cls, layer, element):
        if element == 'all':
            if layer == 0:
                return [cls(**x) for x in DataBase.find(const.COLLECTION_COLONIAS, {})]
            else:
                return [cls(**x) for x in DataBase.find(const.COLLECTION_COLONIAS, {const.ALCALDIA: layer})]
        else:
            return [cls(**x) for x in
                    DataBase.find(const.COLLECTION_COLONIAS, {const.ALCALDIA: layer, const.IDENTIFICADOR: element})]

    @classmethod
    def all_browser(cls, estado, alcaldia, layer):
        if layer == 'colonias':
            if alcaldia == 'all':
                return [cls(**x) for x in DataBase.find(const.COLLECTION_COLONIAS, {const.ESTADO: estado})]
            else:
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_COLONIAS, {const.ESTADO: estado, const.ALCALDIA: alcaldia})]
        elif layer == 'ageb':
            if alcaldia == 'all':
                return [cls(**x) for x in DataBase.find(const.COLLECTION_AGEB, {const.ESTADO: estado})]
            else:
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_AGEB, {const.ESTADO: estado, const.ALCALDIA: alcaldia})]
        elif layer == 'alcaldia':
            return [cls(**x) for x in DataBase.find(const.COLLECTION_ALCALDIA, {const.ESTADO: estado})]

    @classmethod
    def one_byid(cls, identifiador):
        return DataBase.find_one(const.COLLECTION_COLONIAS, {const.IDENTIFICADOR: identifiador})

    @classmethod
    def one_byname(cls, nombre):
        return DataBase.find_one(const.COLLECTION_COLONIAS, {const.NOMBRE: nombre})

    @classmethod
    def one_bycp(cls, cp):
        return DataBase.find_one(const.COLLECTION_COLONIAS, {const.CP.cp: cp})

    def json(self):
        return {
            const.NOMBRE: self.nombre,
            const.ALCALDIA: self.alcaldia,
            const.IDENTIFICADOR: self.identificador,
            const.CP: self.cp,
            const.PROPERTIES: self.properties,
            const.GEOMETRY: self.geometry
        }
