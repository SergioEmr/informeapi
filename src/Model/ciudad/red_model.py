import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class RedVial(object):
    def __init__(self, cvevial, alcaldia, colonia, cuadrante, properties, geometry, _id=None):
        self.cvevial = cvevial
        self.alcaldia = alcaldia
        self.colonia = colonia
        self.cuadrante = cuadrante
        self.properties = properties
        self.geometry = geometry
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_red(datos):
        data = DataBase.find_one(const.COLLECTION_RED, {const.CVEVIAL: datos[const.CVEVIAL]})
        if data is None:
            RedVial(datos[const.CVEVIAL], datos[const.ALCALDIA], datos[const.COLONIA], datos[const.CUADRANTE],
                    datos[const.PROPERTIES], datos[const.GEOMETRY]).save_to_mongo()
            return {"msg": "Registrado correctamente"}
        else:
            return {"msg": "Model o versión no disponible"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_RED, self.json())

    @classmethod
    def all(cls, layer, inside, element):
        if inside == 'all':
            return [cls(**x) for x in DataBase.find(const.COLLECTION_RED, {const.ALCALDIA: layer})]
        else:
            if inside == 'cuadrante':
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_RED, {const.ALCALDIA: layer, const.CUADRANTE: element})]
            elif inside == 'colonia':
                return [cls(**x) for x in
                        DataBase.find(const.COLLECTION_RED, {const.ALCALDIA: layer, const.COLONIA: element})]
            else:
                return {"msg": "Error en el Endpoint: " + inside}

    @classmethod
    def one(cls, cvevial):
        return DataBase.find_one(const.COLLECTION_RED, {const.CVEVIAL: cvevial})

    def json(self):
        return {
            const.CVEVIAL: self.cvevial,
            const.ALCALDIA: self.alcaldia,
            const.COLONIA: self.colonia,
            const.CUADRANTE: self.cuadrante,
            const.PROPERTIES: self.properties,
            const.GEOMETRY: self.geometry
        }
