import pymongo


class DataBase(object):
    URI = 'mongodb://127.0.0.1:27017'
    DB = 'OllyReport'
    DATABASE = None

    @staticmethod
    def init_database():
        client = pymongo.MongoClient(DataBase.URI)
        DataBase.DATABASE = client[DataBase.DB]

    @staticmethod
    def find(collection, query):
        return DataBase.DATABASE[collection].find(query)

    @staticmethod
    def find_one(collection, query):
        return DataBase.DATABASE[collection].find_one(query)

    @staticmethod
    def update(collection, query, data):
        DataBase.DATABASE[collection].update(query, data, upsert=True)

    @staticmethod
    def insert(collection, data):
        DataBase.DATABASE[collection].insert(data)
