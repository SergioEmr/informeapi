import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Cndh(object):
    def __init__(self, fecha_1, status, denunciante, dir_denunciante, parentezco, dir_agraviado, telefono,
                 cargo_persona, agraviado, edad, sexo, nacionalidad, autoridades_responsables, hora_hechos,
                 fecha_hechos, entidad, municipio, lugar_hechos, num_personas, sujetos_uniformados, sujetos_armas,
                 vehiculo_activo, descripcion_vehiculo, activos_delincuencia, pasivo_delincuencia, narracion, lat, lng,
                 _id=None):
        self.fecha_1 = fecha_1
        self.status = status
        self.denunciante = denunciante
        self.dir_denunciante = dir_denunciante
        self.parentezco = parentezco
        self.dir_agraviado = dir_agraviado
        self.telefono = telefono
        self.cargo_persona = cargo_persona
        self.agraviado = agraviado
        self.edad = edad
        self.sexo = sexo
        self.nacionalidad = nacionalidad
        self.autoridades_responsables = autoridades_responsables
        self.hora_hechos = hora_hechos
        self.fecha_hechos = fecha_hechos
        self.entidad = entidad
        self.municipio = municipio
        self.lugar_hechos = lugar_hechos
        self.num_personas = num_personas
        self.sujetos_uniformados = sujetos_uniformados
        self.sujetos_armas = sujetos_armas
        self.vehiculo_activo = vehiculo_activo
        self.descripcion_vehiculo = descripcion_vehiculo
        self.activos_delincuencia = activos_delincuencia
        self.pasivo_delincuencia = pasivo_delincuencia
        self.narracion = narracion
        self.lat = lat
        self.lng = lng
        self._id = uuid.uuid4().hex if _id is None else None

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_CNDH, self.json())

    @staticmethod
    def register(data):
        Cndh(data[const.FECHA_1], data[const.STATUS], data[const.DENUNCIANTE], data[const.DIR_DENUNCIANTE],data[const.PARENTEZCO],
             data[const.DIR_AGRAVIADO], data[const.TELEFONO], data[const.CARGO_PERSONA], data[const.AGRAVIADO],
             data[const.EDAD], data[const.SEXO], data[const.NACIONALIDAD], data[const.AUTORIDADES_RESPONSABLES],
             data[const.HORA_HECHOS], data[const.FECHA_HECHOS], data[const.ENTIDAD], data[const.MUNICIPIO],
             data[const.LUGAR_HECHOS], data[const.NUM_PERSONAS], data[const.SUJETOS_UNIFORMADOS],
             data[const.SUJETOS_ARMAS], data[const.VEHICULO_ACTIVO], data[const.DESCRIPCION_VEHICULO], 
             data[const.ACTIVOS_DELINCUENCIA], data[const.PASIVO_DELINCUENCIA],
             data[const.NARRACION], data[const.LAT], data[const.LNG]).save_to_mongo()
        return {"msg":"Registrado correctamente"}

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_CNDH, {})]

    def json(self):
        return {
            const.FECHA_1: self.fecha_1, const.STATUS: self.status, const.DENUNCIANTE: self.denunciante,
            const.DIR_DENUNCIANTE: self.dir_denunciante, const.PARENTEZCO: self.parentezco,
            const.DIR_AGRAVIADO: self.dir_agraviado, const.TELEFONO: self.telefono,
            const.CARGO_PERSONA: self.cargo_persona, const.AGRAVIADO: self.agraviado, const.EDAD: self.edad,
            const.SEXO: self.sexo, const.NACIONALIDAD: self.nacionalidad,
            const.AUTORIDADES_RESPONSABLES: self.autoridades_responsables, const.HORA_HECHOS: self.hora_hechos,
            const.FECHA_HECHOS: self.fecha_hechos, const.ENTIDAD: self.entidad,
            const.MUNICIPIO: self.municipio, const.LUGAR_HECHOS: self.lugar_hechos,
            const.NUM_PERSONAS: self.num_personas, const.SUJETOS_ARMAS: self.sujetos_armas,
            const.SUJETOS_UNIFORMADOS: self.sujetos_uniformados, const.VEHICULO_ACTIVO: self.vehiculo_activo,
            const.DESCRIPCION_VEHICULO: self.descripcion_vehiculo, const.ACTIVOS_DELINCUENCIA: self.activos_delincuencia,
            const.PASIVO_DELINCUENCIA: self.pasivo_delincuencia, const.NARRACION: self.narracion, const.LAT: self.lat, const.LNG: self.lng
        }
