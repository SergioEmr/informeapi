import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class PredictionModel(object):
    def __init__(self, model_name, model_version, score, x1, x2, x3, x4, x5, scale, _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.score = score
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.x4 = x4
        self.x5 = x5
        self.scale = scale
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_prediction(model_name, model_version):
        data = DataBase.find_one(const.COLLECTION_PREDICTION,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            base = ["#CFD8DC" for x in range(8)]
            PredictionModel(model_name, model_version, const.PENDIENTE, base, base, base, base, base,
                            ["0", "50", "99"]).save_to_mongo()
        else:
            return {"msg": "Ese modelo o versión no está registrado"}

    @staticmethod
    def update_prediction(model_name, model_version, score, x1, x2, x3, x4, x5, iteration):
        data = DataBase.find_one(const.COLLECTION_PREDICTION,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            return {"msg": "Modelo o versión no disponibles"}
        else:
            gradient = lambda \
                x: "#440154" if x <= 0.2 else "#3b528b" if x <= 0.4 else "#21908c" if x <= 0.6 else "#5dc863" if x <= 0.8 else "#fde725"
            data[const.X1][iteration] = gradient(x1)
            data[const.X2][iteration] = gradient(x2)
            data[const.X3][iteration] = gradient(x3)
            data[const.X4][iteration] = gradient(x4)
            data[const.X5][iteration] = gradient(x5)
            print(data[const.X1])
            PredictionModel(data[const.MODEL_NAME], data[const.MODEL_VERSION], score, data[const.X1], data[const.X2],
                            data[const.X3], data[const.X4], data[const.X5], data[const.SCALE]).update_to_mongo()
            return {"msg": "Modelo actualizado"}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_PREDICTION, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_PREDICTION,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_PREDICTION, {})]

    @classmethod
    def one(cls, name, version):
        return DataBase.find_one(const.COLLECTION_PREDICTION, {const.MODEL_NAME: name, const.MODEL_VERSION: version})

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "score": self.score,
            "x1": self.x1,
            "x2": self.x2,
            "x3": self.x3,
            "x4": self.x4,
            "x5": self.x5,
            "scale": self.scale
        }
