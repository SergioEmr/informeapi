import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class Login(object):
    def __init__(self, user, password, _id=None):
        self.user = user
        self.password = password
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def user_register(username, userpassowrd):
        data = DataBase.find_one(const.COLLECTION_USERS, {const.USERNAME: username})
        if data is None:
            Login(username, userpassowrd).save_to_mongo()
            return {'msg': 'Usuario registrado correctamente'}
        else:
            return {'msg': 'Usuario ya registrado'}

    @staticmethod
    def user_login(username, userpassword):
        data = DataBase.find_one(const.COLLECTION_USERS, {const.USERNAME: username, const.PASSWORD: userpassword})
        if data is None:
            return {'msg': 'Usuario no valido'}
        else:
            return {'msg': 'Bienvenido'}

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_USERS, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_USERS, {const.USERNAME: self.user, const.PASSWORD: self.password}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_USERS, {})]

    @classmethod
    def one(cls, username, userpassword):
        return DataBase.find_one(const.COLLECTION_USERS, {const.USERNAME: username, const.PASSWORD: userpassword})

    def json(self):
        return {
            "username": self.user,
            "password": self.password
        }
