from flask_restful import Resource
from src.Model.models_model import ModelsModel
from src.Views.model_response import ModelResponse


class RequestModel(Resource):
    def post(self, name, version, score):
        print(name)
        ModelsModel.update_version(name, version, score)
        return {"msg": "Actualizado correctamente"}, 202

    def get(self, name, version, score):
        return ModelResponse(ModelsModel.all()).getModels(), 202
