import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as constants


class HyperparameterModel(object):
    def __init__(self, model_name, model_version, topology, activation_function, cost_function, learning_rate, epochs,
                 iteration_interval, error_interval, neural_network, dataset_name, dataset_train_demo,
                 dataset_test_demo, dataset_length):
        self.model_name = model_name
        self.model_version = model_version
        self.topology = topology
        self.activation_function = activation_function
        self.cost_function = cost_function
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.iteration_interval = iteration_interval
        self.error_interval = error_interval
        self.neural_network = neural_network
        self.dataset_name = dataset_name
        self.dataset_train_demo = dataset_train_demo
        self.dataset_test_demo = dataset_test_demo
        self.dataset_length = dataset_length


