import uuid
from src.common.ollyreport_database import DataBase
import src.common.constants as const


class TrainingModel(object):
    def __init__(self, model_name, model_version, complete, remaining, velocity, memory, _id=None):
        self.model_name = model_name
        self.model_version = model_version
        self.complete = complete
        self.remaining = remaining
        self.velocity = velocity
        self.memory = memory
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_training(model_name, model_version):
        data = DataBase.find_one(const.COLLECTION_TRAINING,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        hyper = DataBase.find_one(const.COLLECTION_HYPERPARAMETERS,
                                  {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            TrainingModel(model_name, model_version, [0, 0.0], [hyper[const.EPOCHS], 100.0], const.PENDIENTE,
                          const.PENDIENTE).save_to_mongo()

    @staticmethod
    def update_training(model_name, model_version, complete, remaining, velocity, memory):
        data = DataBase.find_one(const.COLLECTION_TRAINING,
                                 {const.MODEL_NAME: model_name, const.MODEL_VERSION: model_version})
        if data is None:
            return {"msg": "Modelo o versión no disponibles"}
        else:
            data[const.COMPLETE] = complete
            data[const.REMAINING] = remaining
            data[const.VELOCITY] = velocity
            data[const.MEMORY] = memory
            TrainingModel(data[const.MODEL_NAME], data[const.MODEL_VERSION], data[const.COMPLETE],
                          data[const.REMAINING], data[const.VELOCITY], data[const.MEMORY]).update_to_mongo()

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_TRAINING, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_TRAINING,
                        {const.MODEL_NAME: self.model_name, const.MODEL_VERSION: self.model_version}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_TRAINING, {})]

    @classmethod
    def one(cls, model, version):
        return DataBase.find_one(const.COLLECTION_TRAINING, {const.MODEL_NAME: model, const.MODEL_VERSION: version})

    def json(self):
        return {
            "model_name": self.model_name,
            "model_version": self.model_version,
            "complete": self.complete,
            "remaining": self.remaining,
            "velocity": self.velocity,
            "memory": self.memory

        }
